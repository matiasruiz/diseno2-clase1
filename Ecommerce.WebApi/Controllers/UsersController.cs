﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ecommerce.WebApi.Controllers
{
    public class UsersController : ApiController
    {
        User[] users = new User[]
        {
            new User { UserId = 1, Name = "Matias", LastName = "Ruiz"},
            new User { UserId = 2, Name = "Renol", LastName = "Manchado"},
            new User { UserId = 3, Name = "Mateo", LastName = "Gallardete"}
        };

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public IHttpActionResult GetUser(int id)
        {
        
            var user = users.FirstOrDefault((u)=>u.UserId==id);
            if(user==null) return NotFound();
            return Ok(user);
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}